import 'package:go_router/go_router.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/routes_manager.dart';
import 'package:flutter/material.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/theme_manager.dart';

class MyApp extends StatefulWidget {
  MyApp._internal(); // private named constructor
  static final MyApp instance =
      MyApp._internal(); // single instance -- singleton

  factory MyApp() => instance; // factory for the class instance

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GoRouter _router = GoRouter(
      routes: RouteManager.getRoute(), initialLocation: Routes.loginRoute);
  // final GoRouter _router = RouteManager.getRoute();
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routeInformationParser: _router.routeInformationParser,
      routerDelegate: _router.routerDelegate,
      title: 'Test Zero One Group (Izza)',
      theme: getApplicationTheme(),
    );
  }
}
