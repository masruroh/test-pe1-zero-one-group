import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/assets_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/color_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/font_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/routes_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/strings_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/styles_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/values_manager.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  late TextEditingController _userNameController = TextEditingController(),
      _passwordController = TextEditingController();
  late bool rememberMe;

  @override
  void initState() {
    super.initState();
    setState(() => rememberMe = false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: ColorManager.white,
        appBar: AppBar(
          title: Text(AppStrings.login,
              style: getBoldStyle(
                  color: ColorManager.white, fontSize: FontSize.s30)),
          centerTitle: false,
          leading: Icon(Icons.keyboard_arrow_left, size: AppSize.s40),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                color: ColorManager.primary,
                width: double.infinity,
                child: Container(
                  child: Center(
                    child: Image.asset(ImageAssets.logoApps,
                        width: MediaQuery.of(context).size.width * 5 / 7,
                        fit: BoxFit.fitWidth),
                  ),
                  decoration: BoxDecoration(
                      color: ColorManager.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(AppSize.s28),
                          topRight: Radius.circular(AppSize.s28))),
                ),
              ),
              Container(
                padding: EdgeInsets.all(AppPadding.p20),
                child: Column(children: [
                  SizedBox(height: AppSize.s16),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(AppStrings.username,
                            style: getBoldStyle(
                                fontSize: FontSize.s18,
                                color: ColorManager.black)),
                        SizedBox(height: AppSize.s4),
                        TextFormField(
                            controller: _userNameController,
                            decoration:
                                InputDecoration(hintText: AppStrings.username),
                            style: getRegularStyle(
                                fontSize: FontSize.s16,
                                color: ColorManager.black)),
                      ],
                    ),
                  ),
                  SizedBox(height: AppSize.s16),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(AppStrings.password,
                            style: getBoldStyle(
                                fontSize: FontSize.s18,
                                color: ColorManager.black)),
                        SizedBox(height: AppSize.s4),
                        TextFormField(
                          controller: _passwordController,
                          decoration:
                              InputDecoration(hintText: AppStrings.password),
                          style: getRegularStyle(
                              fontSize: FontSize.s16,
                              color: ColorManager.black),
                          obscureText: true,
                        ),
                      ],
                    ),
                  ),
                  // SizedBox(height: AppSize.s16),
                  CheckboxListTile(
                    title: Text(AppStrings.rememberMe,
                        style: getRegularStyle(color: ColorManager.darkGrey)),
                    value: rememberMe,
                    onChanged: (newValue) =>
                        setState(() => rememberMe = !rememberMe),
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                    contentPadding: EdgeInsets.all(AppPadding.p0),
                    secondary: TextButton(
                        child: Text(AppStrings.forgetPassword),
                        onPressed: () {}),
                  ),
                  MaterialButton(
                    onPressed: () {},
                    minWidth: double.infinity,
                    padding: EdgeInsets.all(AppPadding.p12),
                    color: ColorManager.primary,
                    child: Text(AppStrings.login,
                        style: getRegularStyle(
                            color: ColorManager.white, fontSize: FontSize.s18)),
                  ),
                  SizedBox(height: AppSize.s60),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Text(AppStrings.goToSignUp),
                    TextButton(
                        child: Text(AppStrings.signUp),
                        onPressed: () => context.go(Routes.registerRoute)),
                  ]),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
