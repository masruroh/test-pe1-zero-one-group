import 'package:test_pe1_zero_one_group/presentation/resources/color_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/font_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/styles_manager.dart';
import 'package:test_pe1_zero_one_group/presentation/resources/values_manager.dart';
import 'package:flutter/material.dart';

ThemeData getApplicationTheme() {
  return ThemeData(
      // main colors of the app
      primaryColor: ColorManager.primary,
      primaryColorLight: ColorManager.primaryOpacity70,
      primaryColorDark: ColorManager.darkPrimary,
      disabledColor: ColorManager.grey1,
      // ripple color
      splashColor: ColorManager.primaryOpacity70,
      // will be used incase of disabled button for example
      // accentColor: ColorManager.grey,
      // card view theme
      cardTheme: CardTheme(
          color: ColorManager.white,
          shadowColor: ColorManager.grey,
          elevation: AppSize.s4),
      // App bar theme
      appBarTheme: AppBarTheme(
          color: ColorManager.primary,
          elevation: AppSize.s0,
          shadowColor: ColorManager.primaryOpacity70,
          titleTextStyle:
              getBoldStyle(color: ColorManager.white, fontSize: FontSize.s30)),
      // Button theme
      buttonTheme: ButtonThemeData(
        // shape: StadiumBorder(),
        disabledColor: ColorManager.grey1,
        buttonColor: ColorManager.primary,
        splashColor: ColorManager.primaryOpacity70,
        textTheme: ButtonTextTheme.primary,
      ),
      textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
              minimumSize: Size.zero,
              padding: EdgeInsets.zero,
              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              alignment: Alignment.centerLeft)),

      // elevated button theme
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              textStyle: getRegularStyle(color: ColorManager.white),
              primary: ColorManager.primary,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(AppSize.s12)))),

      // Text theme
      textTheme: TextTheme(
        headline1:
            getBoldStyle(color: ColorManager.white, fontSize: FontSize.s30),
        subtitle1: getMediumStyle(
            color: ColorManager.lightGrey, fontSize: FontSize.s16),
        subtitle2:
            getMediumStyle(color: ColorManager.primary, fontSize: FontSize.s16),
        caption: getRegularStyle(color: ColorManager.grey1),
        bodyText1: getRegularStyle(color: ColorManager.grey),
      ),

      // input decoration theme (text form field)
      inputDecorationTheme: InputDecorationTheme(
        contentPadding: EdgeInsets.all(AppPadding.p8),
        hintStyle: getRegularStyle(color: ColorManager.black),
        labelStyle: getMediumStyle(color: ColorManager.black),
        errorStyle: getRegularStyle(color: ColorManager.error),

        fillColor: ColorManager.lightGrey, filled: true,
        // enabled border
        enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.grey, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s12))),

        // focused border
        focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.darkGrey, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s8))),

        // error border
        // errorBorder: OutlineInputBorder(
        //     borderSide:
        //         BorderSide(color: ColorManager.error, width: AppSize.s1_5),
        //     borderRadius: BorderRadius.all(Radius.circular(AppSize.s8))),
        // focused error border
        focusedErrorBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.primary, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s8))),
      ));
}
