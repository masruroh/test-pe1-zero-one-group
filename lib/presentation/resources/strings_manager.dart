class AppStrings {
  static const String noRouteFound = "No Route Found";
  static const String login = "Log In";
  static const String register = "Register";

  static const String username = "Username";
  static const String password = "Password";
  static const String rememberMe = "remember me";
  static const String forgetPassword = "Forget Password ? ";
  static const String goToSignUp = "Don't have an account ? ";
  static const String signUp = "Sign Up";

  static const String email = "Email";
  static const String agree = "I agree with ";
  static const String and = " and ";
  static const String terms = "Terms";
  static const String privacy = "Privacy";
  static const String goToLogin = "I'm already an account. ";
  static const String signIn = "Sign In";
}
