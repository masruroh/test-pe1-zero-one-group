import 'package:go_router/go_router.dart';
import 'package:test_pe1_zero_one_group/presentation/login/login.dart';
import 'package:test_pe1_zero_one_group/presentation/register/register.dart';
import 'package:flutter/material.dart';

class Routes {
  static const String loginRoute = "/login";
  static const String registerRoute = "/register";
}

class RouteManager {
  static List<GoRoute> getRoute() {
    return [
      GoRoute(
        path: Routes.loginRoute,
        builder: (BuildContext context, GoRouterState state) =>
            const LoginView(),
      ),
      GoRoute(
        path: Routes.registerRoute,
        builder: (BuildContext context, GoRouterState state) =>
            const RegisterView(),
        // const Page2Screen(),
      ),
    ];
  }
}
