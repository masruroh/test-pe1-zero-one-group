import 'package:flutter/material.dart';

class ColorManager {
  static Color darkPrimary = HexColor.fromHex("#2163ff");
  static Color primary = HexColor.fromHex("#5487ff");
  static Color darkGrey = HexColor.fromHex("#525252");
  static Color grey = HexColor.fromHex("#d2d2d2");
  static Color grey1 = HexColor.fromHex("#eeeeee");
  static Color lightGrey = HexColor.fromHex("#eeeeee");
  static Color primaryOpacity70 = HexColor.fromHex("#B35487ff");

  static Color black = HexColor.fromHex("#000000");
  static Color white = HexColor.fromHex("#FFFFFF");
  static Color error = HexColor.fromHex("#e61f34"); // red color
}

extension HexColor on Color {
  static Color fromHex(String hexColorString) {
    hexColorString = hexColorString.replaceAll('#', '');
    if (hexColorString.length == 6) {
      hexColorString = "FF" + hexColorString; // 8 char with opacity 100%
    }
    return Color(int.parse(hexColorString, radix: 16));
  }
}
